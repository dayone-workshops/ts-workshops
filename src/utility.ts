// type TrainingStore = {
//   [key: UUID]: Training
// }

type TrainingStore = Record<UUID, PersistedTraining>

type MessageInfo = {
  uuid: UUID
  created: Date
  content: string
}

type OptionalMessageInfo = Partial<MessageInfo>

type RequiredMessageInfo = Required<MessageInfo>

type ReadonlyMessage = Readonly<MessageInfo>

type NewMessageInfo = Pick<MessageInfo, 'content'>
type Model = Omit<MessageInfo, 'content'>

