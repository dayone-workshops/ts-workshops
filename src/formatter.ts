type PossiblePlanned = {
  planned?: Date
}

const formatTrainingTime = (training: PossiblePlanned): string | undefined => {
  if (training.planned) {
    return `${training.planned.toLocaleDateString()}`
  }
}

type Entity = {
  uuid: UUID
  created: Date
  updated: Date
}

const formatEntity = (model: Entity): string => {
  return `UUID: ${model.uuid} - created: ${model.created.toDateString} - updated: ${model.updated.toLocaleDateString()}`
}

const m = {
  uuid: "1234",
  created: new Date(),
  updated: new Date()
}

formatEntity(m)
