type Some<T> = {
  _tag: 'Some'
  value: T
}

type None = { _tag: 'None' }

type Option<T> = Some<T> | None

const some = <T>(a: T): Some<T> => ({
  _tag: 'Some',
  value: a,
})

const none = (): None => ({
  _tag: 'None',
})

const map = <A, B>(option: Option<A>, f: (a: A) => B) => {
  return 'value' in option ? some(f(option.value)) : option
}

const flatMap = <A, B>(option: Option<A>, f: (a: A) => Option<B>): Option<B> => {
  return 'value' in option ? f(option.value) : option
}

const getOrElse = <A>(option: Option<A>, fallback: A): A => {
  if (option._tag === 'Some') {
    return option.value
  } else {
    return fallback
  }
}

const ofNullable = <A>(value: A | null | undefined): Option<A> => {
  if (value) {
    return some(value)
  } else {
    return none()
  }
}

const apply = <A>(option: Option<A>, f: (a: A) => void) => {
  return 'value' in option ? f(option.value) : undefined
}

type TrainingDB = Record<UUID, PersistedTraining>
const trainings: TrainingDB = {}

const fetchTraining = (uuid: UUID): Option<PersistedTraining> => {
  return ofNullable(trainings[uuid])
}

const extractExerises = (training: PersistedTraining): Exercise[] => training.exercises
const extractExerciseName = (exercise: Exercise): string => exercise.name

const displayExercisesNames = (uuid: UUID): string => {
  return getOrElse(
    map(map(fetchTraining(uuid), extractExerises), (exercises) => exercises.map(extractExerciseName).join(',')),
    'Try other exercise',
  )
}

type User = {
  uuid: UUID
  name: string
  trainerRef: UUID
}

type UserDB = Record<UUID, User>
const users: UserDB = {}

const fetchUser = (userUUID: UUID): Option<User> => {
  return ofNullable(users[userUUID])
}

const fetchTrainer = (user: User): Option<User> => {
  return fetchUser(user.trainerRef)
}

const fetchTrainingsCreatedBy = (trainer: User): PersistedTraining[] => {
  return Object.values(trainings).filter((t) => t.creatorRef === trainer.uuid)
}

const displayTrainings = (traineeUUID: UUID): PersistedTraining[] => {
  return getOrElse(map(flatMap(fetchUser(traineeUUID), fetchTrainer), fetchTrainingsCreatedBy), [])
}

const parseBirthdayOption = (input: string): Option<Date> => {
  const dateRegexp = /\d{4}-\d{2}-\d{2}/

  if (dateRegexp.test(input)) {
    const date = new Date(input)
    if (date.getTime() < new Date().getTime()) {
      return none()
    }
    return some(date)
  } else {
    return none()
  }
}

const dddd = getOrElse(parseBirthdayOption('WOW'), new Date())
