const parseBirthdayNull = (input: string): Date | null => {
  const dateRegexp = /\d{4}-\d{2}-\d{2}/

  if (dateRegexp.test(input)) {
    const date = new Date(input)
    if (date.getTime() < new Date().getTime()) {
      return null
    }
    return date
  } else {
    return null
  }
}

const parseBirthdayError = (input: string): Date => {
  const dateRegexp = /\d{4}-\d{2}-\d{2}/

  if (dateRegexp.test(input)) {
    const date = new Date(input)
    if (date.getTime() < new Date().getTime()) {
      throw new DateInFutureError()
    }
    return date
  } else {
    throw new InvalidDateInputError()
  }
}

class InvalidDateInputError extends Error {}
class DateInFutureError extends Error {}

type InvalidDateInputErr = {
  code: 'INVALID_DATE_INPUT',
  message?: string
}

type DateInFutureErr = {
  code: 'DATE_IN_THE_FUTURE',
  message?: string
}

const parseBirthdayType = (input: string): Date | InvalidDateInputErr | DateInFutureErr => {
  const dateRegexp = /\d{4}-\d{2}-\d{2}/

  if (dateRegexp.test(input)) {
    const date = new Date(input)
    if (date.getTime() < new Date().getTime()) {
      return {
        code: 'DATE_IN_THE_FUTURE',
        message: 'User provided date from the future'
      }
    }
    return date
  } else {
    return {
      code: 'INVALID_DATE_INPUT',
      message: 'User provided invalid string'
    }
  }
}
