type Config = {
  baseUrl: string
  tier?: Tier
}

type Tier = 'PROD' | 'DEV'

type HttpClient = {
  get: (url: string) => Promise<unknown>
  post: (url: string, data: unknown) => {}
}

const createHttpClient = (c: Config): HttpClient => {
  const config = { tier: 'DEV', ...c }

  if (config.tier === 'DEV') {
    return {
      get: () => {
        return Promise.resolve('wow')
      },
      post: () => {
        return {}
      },
    }
  }

  return {
    get: (url: string) => {
      return fetch(config.baseUrl + url).then((res) => res.json())
    },
    post: (url: string, data: unknown) => {
      return {}
    },
  }
}


const c1 = {
  baseUrl: 'localhost:8000',
  teir: 'PROD'
} as Config
createHttpClient(c1)

const c2 = {
  baseUrl: 'localhost:8000',
  teir: 'PROD' as Tier 
}
createHttpClient(c2)

const c3: Config = {
  baseUrl: 'localhost:8000',
  teir: 'PROD'
}
createHttpClient(c3)


createHttpClient({
  baseUrl: 'localhost:8000',
  teir: 'PROD'
})

createHttpClient({
  baseUrl: 'localhost:8000',
  teir: 'PROD'
} as Config)

createHttpClient({
  baseUrl: 'localhost:8000'
})