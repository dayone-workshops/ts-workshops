type BookFlight = {
  (from: string, to: string, departureDate: Date, returnDate: Date): void
  (from: string, to: string, departure: Date): void
}

type BookFlightAlt = (from: string, to: string, departureDate: Date, returnDate?: Date) => void

const book: BookFlightAlt = (from, to, departureDate, returnDate) => {}
