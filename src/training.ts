type UUID = string

type Exercise = {
  uuid: UUID
  name: string
  description: string
  image: ImageRef
  numberOfReps: number
  numberOfSets: number
  comment?: string
}

type UrlAddress = string

type ImageRef = {
  name: string
  url: UrlAddress
}

type Scale = 1 | 2 | 3 | 4 | 5

type Feedback = {
  happiness: Scale
  difficulty: Scale
}

type TrainingStatus = 'PLANNED' | 'SKIPPED' | 'COMPLETED'

type NewTraining = {
  title?: string
  description?: string
  planned?: Date
  exercises: Exercise[]
}

type RestDay = {
  uuid: UUID
  created: Date
  updated: Date
  creatorRef: UUID
  traineeRef: UUID
  planned: Date
}

type Persisted = {
  uuid: UUID
  created: Date
  updated: Date
}

type PersistedTraining = Persisted &
  NewTraining & {
    creatorRef: UUID
    traineeRef: UUID
    status: 'PLANNED' | 'COMPLETED' | 'SKIPPED'
    feedback?: Feedback
  }


type TrainingInCalendar = Pick<PersistedTraining, 'uuid' | 'title' | 'planned'>

type Workout = NewTraining | RestDay

type WeekDay = 'MON' | 'TUE' | 'WED' | 'THU' | 'FRI' | 'SAT' | 'SUN'

const createWorkout = (day: WeekDay): Workout => {
  if (day === 'SAT' || day === 'SUN') {
    return {} as RestDay
  } else {
    return {} as Training
  }
}
