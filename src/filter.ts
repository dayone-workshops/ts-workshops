type Reduce = <A, B>(collection: A[], f: (acc: B, e: A) => B, init: B) => B

const reduce: Reduce = (array, f, init) => array.reduce(f, init)

reduce([1, 2, 3], (acc, e) => acc + e, 0)

type FlatMap = <A, B>(collectionA: A[], f: (a: A) => B[]) => B[]
type Zip = <A, B>(collectionA: A[], collectionB: B[]) => [A, B][]

function call<T extends unknown[], R>(f: (...args: T) => R, ...args: T): R {
  return f(...args)
}

type Pipe = {
  <A, B>(f: (a: A) => B): (a: A) => B
  <A, B, C>(f: (a: A) => B, ff: (b: B) => C): (a: A) => C
  <A, B, C, D>(f: (a: A) => B, ff: (b: B) => C, fff: (c: C) => D): (a: A) => D
  <A, B, C, D, E>(f: (a: A) => B, ff: (b: B) => C, fff: (c: C) => D, ffff: (d: D) => E): (a: A) => E
}
