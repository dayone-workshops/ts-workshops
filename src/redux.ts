type CompleteTrainingAction = {
  type: 'COMPLETE_TRAINING'
  trainingUUID: UUID
  completed: Date
}

type MoveTrainingAction = {
  type: 'MOVE_TRAINING'
  trainingUUID: UUID
  newDate: Date
}

type GiveFeedbackAction = {
  type: 'GIVE_FEEDBACK'
  trainingUUID: UUID
  feedback: Feedback
}

type SkipTrainingAction = {
  type: 'SKIP_TRAINING'
  trainingUUID: UUID
}

type DeleteTrainingAction = {
  type: 'DELETE_TRAINING'
  trainingUUID: UUID
}

type Action = CompleteTrainingAction | MoveTrainingAction | GiveFeedbackAction | SkipTrainingAction

const reducer = (state: Record<UUID, PersistedTraining>, action: Action) => {
  switch (action.type) {
    case 'COMPLETE_TRAINING':
      return {
        ...state,
        [action.trainingUUID]: { ...state[action.trainingUUID], status: 'COMPLETED', updated: action.completed },
      }
    case 'MOVE_TRAINING':
      return {
        ...state,
        [action.trainingUUID]: { ...state[action.trainingUUID], planned: action.newDate },
      }
    default: {
      return state
    }
  }
}

const reducer2 = (state: Record<UUID, PersistedTraining>, action: Action) => {
  if ('newDate' in action) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], planned: action.newDate },
    }
  } else if ('completed' in action) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], status: 'COMPLETED', updated: action.completed },
    }
  } else if ('feedback' in action) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], feedback: action.feedback },
    }
  }
}

const isCompleteAction = (action: unknown): action is CompleteTrainingAction => {
  if (typeof action === 'object' && action !== null) {
    return (action as CompleteTrainingAction).completed instanceof Date
  }
  return false
}

const isMoveTrainingAction = (action: unknown): action is MoveTrainingAction => {
  if (typeof action === 'object' && action !== null) {
    return (action as MoveTrainingAction).newDate instanceof Date
  }
  return false
}

const isFeedbackAction = (action: unknown): action is GiveFeedbackAction => {
  if (typeof action === 'object' && action !== null) {
    return (action as GiveFeedbackAction).feedback !== undefined
  }
  return false
}

const isSkipAction = (action: unknown): action is SkipTrainingAction => {
  if (typeof action === 'object' && action !== null) {
    return (action as Action).type === 'SKIP_TRAINING'
  }
  return false
}

const reducer3 = (state: Record<UUID, PersistedTraining>, action: Action) => {
  if (isMoveTrainingAction(action)) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], planned: action.newDate },
    }
  } else if (isCompleteAction(action)) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], status: 'COMPLETED', updated: action.completed },
    }
  } else if (isFeedbackAction(action)) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], feedback: action.feedback },
    }
  } else if (isSkipAction(action)) {
    return {
      ...state,
      [action.trainingUUID]: { ...state[action.trainingUUID], status: 'SKIPPED' },
    }
  }
}
