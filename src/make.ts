type Pipe = {
  <A, B>(f: (a: A) => B): (a: A) => B
  <A, B, C>(f: (a: A) => B, ff: (b: B) => C): (a: A) => C
  <A, B, C, D>(f: (a: A) => B, ff: (b: B) => C, fff: (c: C) => D): (a: A) => D
}

///////////

const add10 = (a: number) => a + 10
const hello = (name: string): string => {
  return `Hello ${name}`
}

function call<T extends unknown[], Result>(f: (...ar: T) => Result, ...args: T): Result {
  return f(...args)
}

call(add10, 15)
call(hello, 'Bartek')
